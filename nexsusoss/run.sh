docker run -d \
            --restart always \
            --name nexus \
            --network meete \
            --hostname nexus \
            -p 8081:8081 \
            -p 8123:8123 \
            -p 5000:5000 \
            -v /meete/nexus/nexus-data:/nexus-data \
            sonatype/nexus3