job "ubuntu0" {
  datacenters = [
    "dc1"]
  type = "service"

  constraint {
    attribute = "${node.unique.name}"
    value     = "client1"
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "2m"
    progress_deadline = "5m"
    auto_revert = true
    canary = 1
    auto_promote = true
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "ubuntu0" {
    count = 1


    restart {
      attempts = 2
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      size = 150
    }

    task "ubuntu0" {
      driver = "docker"
      env {
        MASTER_DATABASE_URL = "postgresql://meete:p4ssw0rd@192.168.1.96:5432/affiliate"
        REPLICA_DATABASE_URL = "postgresql://meete:p4ssw0rd@192.168.1.96:5432/affiliate"
        ECHO = "True"
        APP_TITLE = "User Affiliate Service"
        APP_DESCRIPTION = "User Affiliate Service"
        APP_VERSION = "0.1 CLOSED BETA"
        LOG_LEVEL = "DEBUG"
        SERVER_HOST = "0.0.0.0"
        SERVER_PORT = "${NOMAD_HOST_PORT_app_port}"
        USER_INFO_URL = "http://user-voucher-service/userinfo"
        CLIENT_INFO_URL = "http://user-voucher-service/oauth/check_token"
        CONSUL_ENABLED = "True"
        CONSUL_HOST = "localhost"
        CONSUL_PORT = 8500
        CONSUL_SCHEME = "http"
        CONSUL_CACHE_TTL = 60
        REDIS_URL = "192.168.0.23"
        REDIS_PORT = 6379
        AMQP_URL = "amqp://meete:p4ssw0rd@192.168.1.96:5672/meete"
        CONFIG_IMAGE = "fd1a85eed11e.ngrok.io/ubuntu0:latest-dev"
        CONFIG_USERNAME = "admin"
        CONFIG_PASSWORD = "huy123456781"
        CPU = 20
        MEMORY = 64
        MBITS = 20
      }

      config {
        image = "${CONFIG_IMAGE}"

        auth {
          username = "${CONFIG_USERNAME}"
          password = "${CONFIG_PASSWORD}"
        }
        port_map {
          app_port = "${NOMAD_HOST_PORT_app_port}"
        }
        network_mode = "host"
      }

      logs {
        max_files = 2
        max_file_size = 15
      }

      resources {
        cpu = 200
        # 500 MHz
        memory = 1000
        # 256MB
        network {
          mbits = 20
          port "app_port" {}
        }
      }

      service {
        name = "ubuntu0"
        tags = [
          "service"]
        port = "app_port"
      }

      meta {
        BUILD_NUMBER = "${DRONE_BUILD_NUMBER}"
      }
    }
  }
}

